package main

import (
	"math/rand"
)

var availableChars string

// An array of arrays matching a keyboard llayout with integers
// representing the effort `e` in pressing that button; 0 <= e <= 4
var keyboardFitnessLayout [][]int

var strongestKeyboard [][]string

var strongestFitness float64

var test_words = []string{"some", "test", "words"}

const (
	success_threshold       = 10
	chance_of_full_mutation = 72
	max_genes_to_mutate     = 3
)

func totalMutation() bool {
	chance_total_mutation := rand.Intn(chance_of_full_mutation)
	return chance_total_mutation%chance_of_full_mutation == 0
}

func getWordFitness(word string, layout [][]string) int {
	return 1
}

func getTotalFitness(layout [][]string) float64 {
	aggregated_fitness := 0
	for _, word := range test_words {
		aggregated_fitness += getWordFitness(word, layout)
	}
	return 1.0 / float64(aggregated_fitness)
}

func mutateGene(existingLayout [][]string) [][]string {
	newLayout := existingLayout
	// Add 1 so it's never 0 genes to mutate
	chars_to_mutate := rand.Intn(max_genes_to_mutate) + 1
	for i := 0; i <= chars_to_mutate; i++ {
		newLayout = swapChars(newLayout)
	}
	return newLayout
}

func swapChars(currentLayout [][]string) [][]string {
	return [][]string{}
}

func generateParent() [][]string {
	return [][]string{}
}

func generateChild(parent [][]string) [][]string {
	if totalMutation() {
		return generateParent()
	}
	return mutateGene(parent)
}

func main() {

	availableChars = "abcdefghijklmnopqrstuvwxyz"

	keyboardFitnessLayout = [][]int{}
	// Using the standard keyboard layout 10-9-7
	// With fingers resting on first 4 and last 3 letters of middle row.
	keyboardFitnessLayout[0] = []int{4, 3, 2, 2, 3, 3, 2, 3, 3, 4}
	keyboardFitnessLayout[1] = []int{0, 0, 0, 0, 1, 1, 0, 0, 0}
	keyboardFitnessLayout[2] = []int{4, 4, 3, 3, 4, 3, 3}

	dominant_frequency := 0
	thisKeyboard := strongestKeyboard

	if len(strongestKeyboard) == 0 {
		thisKeyboard = generateParent()
	}

	for dominant_frequency < success_threshold {
		this_fitness := getTotalFitness(thisKeyboard)
		if this_fitness > strongestFitness {
			dominant_frequency = 0
			strongestFitness = this_fitness
			strongestKeyboard = thisKeyboard
		}
	}
	print(strongestKeyboard)
}
